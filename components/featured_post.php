<?php
// server component

add_action( 'init', 'guten_blocks_register_block_featured_post' );

function guten_blocks_register_block_featured_post() {

    // Only load if Gutenberg is available.
	if ( ! function_exists( 'register_block_type' ) ) {
		return;
    }
    
    // Hook server side rendering into render callback
    register_block_type( 'gutenberg-blocks/featured-post', array(
        'render_callback' => 'guten_blocks_render_block_featured_post',
    ) );
}

function guten_blocks_render_block_featured_post( $attributes, $content ) {
	$query_args = array(
		'numberposts' => 1,
		'post_status' => 'publish',
		'meta_query'  => array(
			array(
				'key' => 'post_show_on_homepage',
				'compare' => '=',
				'value' => '1'
			)
		)
	);
    $posts_list = get_posts( $query_args );
	if ( empty( $posts_list ) ) {
		return '';
	}
	
	global $post;
	$post = $posts_list[ 0 ];
	setup_postdata( $post );
	$markup = sprintf(
		'<article class="featured-post"><a href="%1$s"><img class="img-fluid" src="%3$s"></a><h3 class="entry-title"><a href="%1$s">%2$s</a></h3>%4$s</article>',
		esc_url( get_permalink( $post->ID ) ),
		esc_html( get_the_title( $post->ID ) ),
		esc_url( get_the_post_thumbnail_url( $post->ID, 'post-featured' ) ),
		( get_the_excerpt( $post->ID ) )
	);
	wp_reset_postdata();
	return $markup;
}