<?php
// server component

add_action( 'init', 'guten_blocks_register_block_events' );

function guten_blocks_register_block_events() {

    // Only load if Gutenberg is available.
	if ( ! function_exists( 'register_block_type' ) ) {
		return;
    }
    
    // Hook server side rendering into render callback
    register_block_type( 'gutenberg-blocks/events', array(
        'render_callback' => 'guten_blocks_render_block_events',
    ) );
}

function guten_blocks_render_block_events( $attributes, $content ) {
    $query_args = array(
		'posts_per_page' => 4,
		'post_status'	 => 'publish',
		'post_type' 	 => 'events',
		'meta_key' 		 => 'events_start_date',
		'orderby'		 => 'meta_value_num',
		'meta_query'	 => array(
			array(
				'key' => 'events_show_in_gutenberg',
				'compare' => '=',
				'value' => '1'
			)
		)
		
	);
    $posts_list = new WP_Query( $query_args );
	if ( empty( $posts_list ) ) {
		return '';
	}

	$markup = '<div class="events-block">';

	while( $posts_list->have_posts() ) {
		$posts_list->the_post();
		$markup .= \App\template('partials.content-events');
	}
	wp_reset_postdata();

	$markup .= '</div>';

	return $markup;
}
