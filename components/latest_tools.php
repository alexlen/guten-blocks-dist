<?php
// server component

add_action( 'init', 'guten_blocks_register_block_latest_tools' );

function guten_blocks_register_block_latest_tools() {

    // Only load if Gutenberg is available.
	if ( ! function_exists( 'register_block_type' ) ) {
		return;
    }
    
    // Hook server side rendering into render callback
    register_block_type( 'gutenberg-blocks/latest-tools', array(
        'render_callback' => 'guten_blocks_render_block_latest_tools',
    ) );
}

function guten_blocks_render_block_latest_tools( $attributes, $content ) {
    $recent_posts = wp_get_recent_posts( [
        'numberposts' => 3,
        'post_type' => 'tool',
		'post_status' => 'publish',
	] );
	if ( empty( $recent_posts ) ) {
		return '';
	}
	$markup = '<div class="row lg-gutter latest-tools">';
	foreach ( $recent_posts as $post ) {
		$post_id  = $post['ID'];
		$markup  .= sprintf(
			'<div class="col-md">
				<div class="d-flex flex-md-wrap flex-xl-nowrap">
					<a href="%1$s" class="mr-3 my-2">
						<img class="rounded-circle img-thumbnail tool-logo" src="%3$s" alt="" width="90" height="90">
					</a>
					<div>
						<a href="%1$s"><h6>%2$s</h6></a>
						<p>%4$s</p>
					</div>
				</div>
			</div>',
			esc_url( get_permalink( $post_id ) ),
			esc_html( get_the_title( $post_id ) ),
			esc_url( get_field( "tool_logo", $post_id ) ),
			esc_html( get_field( "tool_description", $post_id ) )
		);
	}
	$markup  .= '</div>';
	return "{$markup}";
}