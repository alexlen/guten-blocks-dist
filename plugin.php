<?php
/**
 * Plugin Name: Guten Blocks
 * Plugin URI: https://github.com/ahmadawais/create-guten-block/
 * Description: Custom Gutenberg blocks - a Gutenberg plugin created via create-guten-block.
 * Author: Altom
 * Author URI: https://altom.com/
 * Version: 1.0.0
 * License: GPL2+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Add server components
require_once plugin_dir_path( __FILE__ ) . 'components/latest_tools.php';
require_once plugin_dir_path( __FILE__ ) . 'components/featured_post.php';
require_once plugin_dir_path( __FILE__ ) . 'components/events.php';

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * `wp-blocks`: includes block type registration and related functions.
 *
 * @since 1.0.0
 */
function guten_blocks_cgb_block_assets() {
	// Styles.
	wp_enqueue_style(
		'guten_blocks-cgb-style-css', // Handle.
		plugins_url( 'dist/blocks.style.build.css', __FILE__ ), // Block style CSS.
		array( 'wp-editor' ) // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: filemtime — Gets file modification time.
	);
} // End function guten_blocks_cgb_block_assets().

// Hook: Frontend assets.
add_action( 'enqueue_block_assets', 'guten_blocks_cgb_block_assets' );

/**
 * Enqueue Gutenberg block assets for backend editor.
 *
 * `wp-blocks`: includes block type registration and related functions.
 * `wp-element`: includes the WordPress Element abstraction for describing the structure of your blocks.
 * `wp-i18n`: To internationalize the block's text.
 *
 * @since 1.0.0
 */
function guten_blocks_cgb_editor_assets() {
	// Scripts.
	wp_enqueue_script(
		'guten_blocks-cgb-block-js', // Handle.
		plugins_url( '/dist/blocks.build.js', __FILE__ ), // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'jquery', 'wp-components', 'wp-editor' ), // Dependencies, defined above.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
		true // Enqueue the script in the footer.
	);

	// Styles.
	wp_enqueue_style(
		'guten_blocks-cgb-block-editor-css', // Handle.
		plugins_url( 'dist/blocks.editor.build.css', __FILE__ ), // Block editor CSS.
		array( 'wp-edit-blocks' ) // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: filemtime — Gets file modification time.
	);
} // End function guten_blocks_cgb_editor_assets().

// Hook: Editor assets.
add_action( 'enqueue_block_editor_assets', 'guten_blocks_cgb_editor_assets' );
